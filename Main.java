import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		DFA test= new DFA();
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the string that you want to check: ");
		String check=input.nextLine();
		System.out.println(test.accept(check));
	}
}
