
/**
 * A list of states
 * @author Geanina Tambaliuc
 *
 */
public enum States {
	q0(false),q1(false),q2(false),q3(false),q4(true),
	q5(false);
	
	final boolean accept;
	
	/**
	 * Constructor for states
	 * @param accept
	 */
	States(boolean accept)
	{
		this.accept=accept;
	}
	
	States zero;
	States one;
	
	//transitions
	static {
		q0.zero=q1; q0.one=q0;
		q1.zero=q0; q1.one=q2;
		q2.zero=q3; q2.one=q1;
		q3.zero=q4; q3.one=q1;
		q4.zero=q4; q4.one=q1;
		
	}
	
	/**
	 * This method makes the transitions
	 * @param ch 0/1 from the alphabet
	 * @return 
	 */
	States transition(char ch)
	{
		switch (ch)
		{
		case '0':
			return this.zero == null? q5: this.zero;
		case '1':
			return this.one == null? q5:this.one;
		default:
			return q5;
		}
	}
}
