import java.util.Scanner;

/**
 * This class creates a DFA
 * @author Geanina Tambaliuc
 *
 */
public class DFA {

	
	/**
	 * This method checks if a string is accepted or not
	 * @param s the binary string
	 * @return true/false
	 */
	public boolean accept(String s)
	{
		//q4 is the initial state
		States state= States.q4;
		
		for(int i=0;i< s.length();i++)
		{
			state=state.transition(s.charAt(i));
		}
		return state.accept;
	}
	
	
}
