import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DFATest {

	@Test
	void test1() {
		DFA dfa1=new DFA();
		// 1100 is 12. So the result should be true
		assertTrue(dfa1.accept("1100")==true);
	}
	
	@Test
	void test2() {
		DFA dfa2=new DFA();
		// 1010 is 10. So the result should be false
		assertTrue(dfa2.accept("1010")==false);
	}
	
	@Test
	void test3() {
		DFA dfa3=new DFA();
		// 1001 is 9. So the result should be false
		assertTrue(dfa3.accept("1001")==false);
	}
	
	@Test
	void test4() {
		DFA dfa4=new DFA();
		// 100 is 4. So the result should be false
		assertTrue(dfa4.accept("100")==false);
	}

	@Test
	void test5() {
		DFA dfa5=new DFA();
		// 11 is 3. So the result should be false
		assertTrue(dfa5.accept("11")==false);
	}
	
	@Test
	void test6() {
		DFA dfa6=new DFA();
		// 11000 is 24. So the result should be true
		assertTrue(dfa6.accept("11000")==true);
	}
	
	@Test
	void test7() {
		DFA dfa7=new DFA();
		// 110000 is 48. So the result should be true
		assertTrue(dfa7.accept("110000")==true);
	}
}
